export const state = () => ({
  properties: {}
})

export const mutations = {
  setProperties (state, props) {
    state.properties = props
  }
}
